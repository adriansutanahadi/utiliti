class StaticPagesController < ApplicationController
  def home
    @latest_listings = Listing.first(4)
  end

  def help
  end

  def about
  end

  def contact
  end
end
