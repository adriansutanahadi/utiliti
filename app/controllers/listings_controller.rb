class ListingsController < ApplicationController
  before_action :logged_in_user, only: [:new, :create, :edit, :update, :destroy]
  before_action :correct_user,   only: [:edit, :update, :destroy]

  def show
    @listing = Listing.find(params[:id])
    @listing_attachments = @listing.listing_attachments.all
  end

  def new
  	@listing = Listing.new
  	@listing_attachment = @listing.listing_attachments.build
  end

  def edit
    # @listing = Listing.find(params[:id])
    @listing_attachment = @listing.listing_attachments.build
    @listing_pic_attachments = @listing.listing_attachments.where(attachment_group: 'picture')
  end

  def destroy
    @listing.destroy
    flash[:success] = "Listing successfully deleted."
    # redirect_to request.referrer || root_url
    redirect_to current_user
  end

  def create
    @listing = current_user.listings.build(listing_params)
    if @listing.save
      unless params[:listing_pic_attachments].nil?
        params[:listing_pic_attachments]['image'].each do |a|
            @listing_attachment = @listing.listing_attachments.create!(image: a, attachment_group: 'picture')
        end
      end
      flash[:success] = "Listing successfully created."
      redirect_to @listing
    else
      render 'new'
    end
  end

  def update
    # @listing = Listing.find(params[:id])
    if @listing.update_attributes(listing_params)
      flash[:success] = "Listing successfully updated."
      redirect_to @listing
    else
      render 'edit'
    end
  end

  def search
  end

  private

    def listing_params
      params.require(:listing).permit(:name, :brand, :modeltype, :origin, :hourmeter, :year, :price, :condition, :sell_status, :lease_status, :location, :description,
                                        post_attachments_attributes: [:id, :listing_id, :image, :attachment_group])
    end

    def correct_user
      @listing = current_user.listings.find_by(id: params[:id])
      redirect_to root_url if @listing.nil?
    end
end
