class AccountActivationsController < ApplicationController

  def edit
    user = User.find_by(email: params[:email])
    if user && !user.activated? && user.authenticated?(:activation, params[:id])
      user.activate
      log_in user
      flash[:success] = "Account successfully activated!"
      redirect_to user
    else
      flash[:danger] = "Invalid activation link"
      redirect_to root_url
    end
  end

  def new_resend_activation
  end

  def resend_activation
    user = User.find_by(email: params[:resend_activation][:email])
    if user
      user.resend_activation_email
      flash[:info] = "Please check your email to activate your account."
      redirect_to login_url
    else
      flash[:danger] = "There was no account found for your e-mail address."
      redirect_to resend_activation_url
    end
  end

end