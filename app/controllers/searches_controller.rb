class SearchesController < ApplicationController

  def new
    @search = Search.new
    # @names = Listing.reorder('name asc').uniq.pluck(:name)
    # @brands = Listing.reorder('brand asc').uniq.pluck(:brand)
    @page = params[:page]
  end

  def create
    @search = Search.create(search_params)
    redirect_to @search
  end

  def show
    @search = Search.find(params[:id])
  end

  private
    def search_params
      params.require(:search).permit(:modeltype, :origin, :min_hourmeter, :max_hourmeter, :min_year, :max_year, :min_price, :max_price, :condition, :sell_status, :lease_status, :location, :name, brand:[])
    end
end
