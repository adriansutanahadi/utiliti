class ListingAttachmentsController < ApplicationController
  before_action :logged_in_user, only: [:destroy]
  before_action :correct_user,   only: [:destroy]

  def destroy
    listing = @listing_attachment.listing
    @listing_attachment.destroy
    flash[:success] = "Attachment deleted"
    redirect_to request.referrer || listing
  end

  private

    def correct_user
      @listing_attachment = ListingAttachment.find(params[:id])
      @listing = current_user.listings.find_by(id: @listing_attachment.listing.id)
      redirect_to root_url if @listing.nil?
    end
end
