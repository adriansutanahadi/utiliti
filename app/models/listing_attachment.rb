class ListingAttachment < ActiveRecord::Base
  mount_uploader :image, ImageUploader
  belongs_to :listing
  validate  :image_size
  validates :attachment_group, presence: true

  private

    # Validates the size of an uploaded image.
    def image_size
      if image.size > 5.megabytes
        errors.add(:image, "should be less than 5MB")
      end
    end

end
