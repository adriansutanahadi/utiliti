class Listing < ActiveRecord::Base
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  has_many :listing_attachments, dependent: :destroy
  accepts_nested_attributes_for :listing_attachments
  validates :user_id, presence: true

  def title
  	self.name + ' ' +  self.brand + ' (' + self.year.to_s + ')'
  end
end
