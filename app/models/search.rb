class Search < ActiveRecord::Base
  attr_accessor :last_search
  before_save do
    self.name.gsub!(/[\[\]\"]/, "") if attribute_present?("name")
    self.brand.gsub!(/[\[\]\"]/, "") if attribute_present?("brand")
  end

  def search_listings

    listings = Listing.all

    listings = listings.where(["name IN (?)", name.split(/\s*,\s*/)]) if name.present?
    listings = listings.where(["brand IN (?)", brand.split(/\s*,\s*/)]) if brand.present?
    listings = listings.where(["modeltype LIKE ?", modeltype]) if modeltype.present?
    listings = listings.where(["origin LIKE ?", origin]) if origin.present?
    listings = listings.where(["hourmeter >= ?", min_hourmeter]) if min_hourmeter.present?
    listings = listings.where(["hourmeter <= ?", max_hourmeter]) if max_hourmeter.present?
    listings = listings.where(["year >= ?", min_year]) if min_year.present?
    listings = listings.where(["year <= ?", max_year]) if max_year.present?
    listings = listings.where(["price >= ?", min_price]) if min_price.present?
    listings = listings.where(["price <= ?", max_price]) if max_price.present?
    listings = listings.where(["condition LIKE ?", condition]) if condition.present?
    listings = listings.where(["sell_status = ?", sell_status]) if sell_status.present?
    listings = listings.where(["lease_status = ?", lease_status]) if lease_status.present?
    listings = listings.where(["location LIKE ?", location]) if location.present?
    
    listings = listings.paginate(page: @page)
    self.last_search = listings
    return self.last_search
  end
end
