$(document).on("turbolinks:load", function() {
  // The slider being synced must be initialized first
  $('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 210,
    itemMargin: 5,
    asNavFor: '#slider'
  });
 
  $('#slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: true,
    slideshow: false,
    sync: "#carousel",
    start: function(slider) {
      $('.current-slide').text(slider.currentSlide+1);
      $('.total-slides').text(slider.count);
    },
    after: function(slider) {
      $('.current-slide').text(slider.currentSlide+1);
    }
  });
});