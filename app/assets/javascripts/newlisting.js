$(document).on("turbolinks:load", function() {
  var $step1 = $('#step1');
  var $step2 = $('#step2');
  var $step3 = $('#step3');
  var $allBrandsInput = $('#step2 input[id*="_brand_"]');
  var $allBrandsLabel = $('#step2 label[for*="_brand_"]');

  var removeAllBrands = function() {
    // uncheck selection
    $allBrandsInput.each(function() {
      $(this).prop('checked', false);
    });

    // remove label
    $allBrandsLabel.css('display', 'none');
  };

  var catalog = new Object();

  catalog["Excavator"] = ["Komatsu", "Hitachi", "Caterpillar", "Kobelco", "Volvo", "Sumitomo", "JCB", "Hyundai", "Takeuchi", "Keihatsu", "Bobcat", "Terex", "Kubota", "Case", "Shantui", "Liu Gong", "Sany", "XCMG"];
  catalog["Bulldozer"] = ["Komatsu", "Caterpillar", "Hitachi", "XCMG", "Shantui", "Liu Gong", "Sany"];
  catalog["Motor Grader"] = ["Komatsu", "Caterpillar", "Volvo", "XCMG", "Shantui", "Liu Gong", "Sany"];
  catalog["Wheel Loader"] = ["Komatsu", "Caterpillar", "Hitachi", "Kubota", "Case", "Bobcat", "Hyundai", "JCB", "John Deere", "New Holland", "Volvo", "XCMG", "Liu Gong"];
  catalog["Backhoe Loader"] = ["Hitachi", "Case", "Caterpillar", "John Deere", "Komatsu", "New Holland", "Terex", "Volvo", "Liu Gong"];
  catalog["Tractor"] = ["New Holland", "Massey Ferguson", "Kubota", "John Deere", "Mitsubishi"];
  catalog["Compactor/Roller"] = ["Sakai", "Bomag"];
  catalog["Forklift"] = ["Toyota", "Mitsubishi", "Caterpillar", "Komatsu", "Hyundai", "Lonking"];
  catalog["Crane"] = ["Tadano", "Kato"];
  catalog["Asphalt Finisher"] = ["Komatsu", "Caterpillar", "Bomag", "Sumitomo"];

  $step2.css('display', 'none');
  $step3.css('display', 'none');

  $('#step1 input').on('change', function() {
    var availableBrands = catalog[$("input:checked").next().html()];

    removeAllBrands();
    for (var i = 0; i < availableBrands.length; i++) {  
      $('#step2 label:contains("' + availableBrands[i] + '")').fadeIn();
    };
    $step2.fadeIn();
  });

  $('#step2 input').on('change', function() {
    $step3.fadeIn();
  });

  // trigger autonumeric
  $(document).trigger('refresh_autonumeric');
});