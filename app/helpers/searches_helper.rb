module SearchesHelper
  def checked_name(area)
    @search.name.nil? ? false : @search.name.match(area)
  end

  def checked_brand(area)
    @search.brand.nil? ? false : @search.brand.match(area)
  end
end
