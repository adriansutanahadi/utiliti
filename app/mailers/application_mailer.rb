class ApplicationMailer < ActionMailer::Base
  default from: "noreply@utiliti.id"
  layout 'mailer'
end
