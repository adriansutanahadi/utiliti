class ChangePriceToBigInt < ActiveRecord::Migration[5.0]
  def change
  	change_column :listings, :price, :integer, limit: 8
  end
end
