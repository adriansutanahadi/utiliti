class CreateSearches < ActiveRecord::Migration
  def change
    create_table :searches do |t|
      t.string :name
      t.string :brand
      t.string :modeltype
      t.integer :min_year
      t.integer :max_year
      t.integer :min_hourmeter
      t.integer :max_hourmeter
      t.string :origin
      t.integer :min_price
      t.integer :max_price
      t.string :location
      t.string :condition
      t.boolean :sell_status
      t.boolean :lease_status

      t.timestamps null: false
    end
  end
end
