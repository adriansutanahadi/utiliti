class AddStatusToListing < ActiveRecord::Migration
  def change
    add_column :listings, :sell_status, :boolean, default: false
    add_column :listings, :lease_status, :boolean, default: false
  end
end
