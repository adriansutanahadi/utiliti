class CreateListingAttachments < ActiveRecord::Migration
  def change
    create_table :listing_attachments do |t|
      t.string :image
      t.references :listing, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :listing_attachments, [:listing_id, :created_at]
  end
end
