class AddAttachmentGroupToListingAttachment < ActiveRecord::Migration
  def change
    add_column :listing_attachments, :attachment_group, :string
  end
end
