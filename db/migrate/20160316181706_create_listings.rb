class CreateListings < ActiveRecord::Migration
  def change
    create_table :listings do |t|
      t.string :name
      t.string :brand
      t.string :modeltype
      t.integer :year
      t.integer :hourmeter
      t.string :origin
      t.integer :price
      t.string :location
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :listings, [:user_id, :created_at]
  end
end
