# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160919145749) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "listing_attachments", force: :cascade do |t|
    t.string   "image",            limit: 255
    t.integer  "listing_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "attachment_group", limit: 255
    t.index ["listing_id", "created_at"], name: "index_listing_attachments_on_listing_id_and_created_at", using: :btree
    t.index ["listing_id"], name: "index_listing_attachments_on_listing_id", using: :btree
  end

  create_table "listings", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.string   "brand",        limit: 255
    t.string   "modeltype",    limit: 255
    t.integer  "year"
    t.integer  "hourmeter"
    t.string   "origin",       limit: 255
    t.bigint   "price"
    t.string   "location",     limit: 255
    t.integer  "user_id"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "condition",    limit: 255
    t.boolean  "sell_status",              default: false
    t.boolean  "lease_status",             default: false
    t.text     "description"
    t.index ["user_id", "created_at"], name: "index_listings_on_user_id_and_created_at", using: :btree
    t.index ["user_id"], name: "index_listings_on_user_id", using: :btree
  end

  create_table "searches", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.string   "brand",         limit: 255
    t.string   "modeltype",     limit: 255
    t.integer  "min_year"
    t.integer  "max_year"
    t.integer  "min_hourmeter"
    t.integer  "max_hourmeter"
    t.string   "origin",        limit: 255
    t.integer  "min_price"
    t.integer  "max_price"
    t.string   "location",      limit: 255
    t.string   "condition",     limit: 255
    t.boolean  "sell_status"
    t.boolean  "lease_status"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.string   "email",             limit: 255
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.string   "password_digest",   limit: 255
    t.string   "remember_digest",   limit: 255
    t.boolean  "admin",                         default: false
    t.string   "activation_digest", limit: 255
    t.boolean  "activated",                     default: false
    t.datetime "activated_at"
    t.string   "reset_digest",      limit: 255
    t.datetime "reset_sent_at"
    t.string   "username",          limit: 255
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["username"], name: "index_users_on_username", using: :btree
  end

  add_foreign_key "listing_attachments", "listings"
  add_foreign_key "listings", "users"
end
